# NAME

Text::TinySegmenter::Fast - A fast and compact Japanese tokenizer ports from `tinysegmenter.jl` ([https://github.com/chezou/TinySegmenter.jl](https://github.com/chezou/TinySegmenter.jl))

# SYNOPSIS

    use strict;
    use warnings;
    use utf8;
    
    use Text::TinySegmenter::Fast;
    
    my $src     = q{今日は良い天気ですね};
    
    my @tokens  = Text::TinySegmenter::Fast->tokenize($src);
    # => qw[ 今日 は 良い 天気 です ね ];

# DESCRIPTION

[Text::TinySegmenter::Fast](https://metacpan.org/pod/Text::TinySegmenter::Fast) is a fast and compact Japanese tokenizer.

Algorithm of this library is based on Julia-lang version of TinySegmenter ([https://github.com/chezou/TinySegmenter.jl](https://github.com/chezou/TinySegmenter.jl)),
and that library is optimized for more fast tokenize from Japanese text.

# BENCHMARK

Benchmark script includes at `eg/benchmark.pl` inside repository of this library,
and my result of benchmark as follows:

## Hardware and Environment

    Hardware: Dell XPS 9560 4k model with Japanese keyboard
    OS: Windows 10 Pro 64bit Fall Creators Update
    Environment: Archlinux on Windows Subsystem for Linux (replaced from Ubuntu) 

## Result

    $ perl -I lib eg/benchmark.pl
                                 Rate  Text::TinySegmenter Text::TinySegmenter::Fast
    Text::TinySegmenter        6711/s                   --                      -66%
    Text::TinySegmenter::Fast 19608/s                 192%                        --

# COPYRIGHTS

## `TinySegmenter` [http://chasen.org/~taku/software/TinySegmenter/](http://chasen.org/~taku/software/TinySegmenter/) (original)

    Copyright (c) 2008, Taku Kudo
    
    All rights reserved.
    
    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

      * Redistributions of source code must retain the above copyright notice,
        this list of conditions and the following disclaimer.

      * Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in the
        documentation and/or other materials provided with the distribution.
      
      * Neither the name of the <ORGANIZATION> nor the names of its
        contributors may be used to endorse or promote products derived from this
        software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
    "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
    LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
    A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
    CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
    EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
    PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
    PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
    LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
    NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

## `TinySegmenter.jl` [https://github.com/chezou/TinySegmenter.jl](https://github.com/chezou/TinySegmenter.jl) (A julia-lang ports)

    The TinySegmenter.jl package is licensed under the 3-claused BSD License:
    Copyright (c) 2015, Taku Kudo, Michiaki Ariga All rights reserved.
    
    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:
    
    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.

    * Neither the name of the nor the names of its
      contributors may be used to endorse or promote products derived from this
      software without specific prior written permission.
    
    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
    "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
    LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
    A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER
    BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
    EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
    PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
    OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
    WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
    OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
    EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# LICENSE

    Copyright (C) 2017 Naoki OKAMURA 

    Redistribution and use in source and binary forms,
    with or without modification, are permitted provided
    that the following conditions are met:
     
    1. Redistributions of source code must retain the above copyright notice,
       this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
       contributors may be used to endorse or promote products derived
       from this software without specific prior written permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
    "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
    THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
    ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
    FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
    DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
    OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
    OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
    EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# AUTHOR

Naoki OKAMURA a.k.a nyarla <nyarla@thotep.net>

# SEE ALSO

[http://chasen.org/~taku/software/TinySegmenter/](http://chasen.org/~taku/software/TinySegmenter/)

[https://github.com/chezou/TinySegmenter.jl/](https://github.com/chezou/TinySegmenter.jl/)

[https://github.com/nyarla/p5-Text-TinySegmenter-Fast/](https://github.com/nyarla/p5-Text-TinySegmenter-Fast/)
