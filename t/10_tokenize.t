use strict;
use warnings;
use utf8;

use Test::More;

use Text::TinySegmenter::Fast;

my @msg = Text::TinySegmenter::Fast->tokenize('今日は良い天気ですね');
my @ret = qw[ 今日 は 良い 天気 です ね ];

is(
  join(q{|}, @msg),
  join(q{|}, @ret),
);

done_testing;
