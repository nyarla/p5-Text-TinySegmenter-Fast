#!/usr/bin/env perl

use strict;
use warnings;
use utf8;

use Benchmark qw[ cmpthese ];

use Text::TinySegmenter;
use Text::TinySegmenter::Fast;

my $text = q{今日は良い天気ですね};

sub tinysegmenter {
  Text::TinySegmenter->segment($text);
}

sub tinysegmenter_fast {
  Text::TinySegmenter::Fast->tokenize($text);
}

cmpthese(10000, {
  'Text::TinySegmenter' => \&tinysegmenter,
  'Text::TinySegmenter::Fast' => \&tinysegmenter_fast,  
});
